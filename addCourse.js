let addCourseForm = document.querySelector("#createCourse")

addCourseForm.addEventListener("submit", (e) => {
    e.preventDefault();

    let courseName = document.querySelector("#courseName").value;
    let coursePrice = document.querySelector("#coursePrice").value;
    let courseDescription = document.querySelector("#courseDescription").value;

    fetch("http://localhost:4000/courses/",{
        method: "POST",
        headers: {
            "Content-Type" : "application/json",
            Authorization:`Bearer ${token}`
        },
        body: JSON.stringify({
            name: courseName,
            description: courseDescription,
            price: coursePrice
        })
    })
    .then(res => res.json())
    .then(data => {
        if(data === true){
            alert("Successfully add New Course")
            window.location.replace("./courses.html")
        }else{
            alert("Unsuccessful. Please try again.")
        }
    })

})